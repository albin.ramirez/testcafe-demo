import { Selector } from 'testcafe';

const userName = 'standard_user';
const userPass = 'secret_sauce';

fixture `Start Page`
    .page `https://www.saucedemo.com/`;

test('Login Test', async t => {
    await t
        .typeText('[data-test=username]', userName)
        .typeText('[data-test=password]', userPass)
        .click('[type=submit]');
    await Selector('.product_label');
});